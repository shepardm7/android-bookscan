package com.example.jaypatel.bookscan.data;

import com.example.jaypatel.bookscan.NoteModel;

import java.util.ArrayList;

public class CheckoutList {
    private static ArrayList<NoteModel> data = new ArrayList<>();
    public static void setData(String barcode_id, String aResult, String book_id) {
        data.add(new NoteModel(barcode_id, aResult, book_id));
    }
    public static ArrayList<NoteModel> getData() {
        return data;
    }
}
