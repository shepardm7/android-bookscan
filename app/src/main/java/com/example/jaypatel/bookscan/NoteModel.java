package com.example.jaypatel.bookscan;

/**
 * Created by Admin on 2016-12-20.
 */

public class NoteModel {
    String barcode_id;
    String Return;
    String book_id;
    int _id;
    String User_name;
    String user_email;

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public NoteModel(String barcode_id, String aReturn) {
        this.barcode_id = barcode_id;
        Return = aReturn;
    }

    public NoteModel(String barcode_id, String aReturn, String book_id) {
        this.barcode_id = barcode_id;
        Return = aReturn;
        this.book_id = book_id;
    }

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getReturn() {
        return Return;
    }

    public void setReturn(String aReturn) {
        Return = aReturn;
    }


    public int get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public NoteModel(String barcode_id) {
        this.barcode_id = barcode_id;
    }

    public NoteModel() {

    }

    public String getUser_name() {
        return User_name;
    }

    public void setUser_name(String user_name) {
        User_name = user_name;
    }

    public String getBarcode_id() {
        return barcode_id;
    }

    public void setBarcode_id(String barcode_id) {
        this.barcode_id = barcode_id;
    }
}
