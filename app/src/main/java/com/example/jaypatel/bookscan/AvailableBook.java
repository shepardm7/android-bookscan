package com.example.jaypatel.bookscan;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Admin on 24-04-2018.
 */
public class AvailableBook  extends Activity{

    String bookname,author;
    TextView bookav,authorname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.availablebook);
        bookname = getIntent().getStringExtra("bookname").toString();
        bookav= (TextView)findViewById(R.id.book);
        authorname=(TextView)findViewById(R.id.author);
        authorname.setText("Author Name:"+getIntent().getStringExtra("author").toString());
        bookav.setText("Avaliable:"+bookname);
    }
    }
