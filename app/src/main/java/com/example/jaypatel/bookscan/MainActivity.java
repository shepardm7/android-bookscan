package com.example.jaypatel.bookscan;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
   // private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public DataBaseHandler db;
    CoordinatorLayout coordinatorLayout;
    String barcode_id;
    Bundle bundle;
    TabLayout tabLayout;
    private static ArrayList<NoteModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.main_content);

        db = new DataBaseHandler(this);
        data = new ArrayList<>();

       /* if (savedInstanceState == null) {
            // put fragment to activity layout
            SettingAccount fragment = new SettingAccount();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_content, fragment, "fragment");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }*/
      /*  Bundle intent = getIntent().getExtras();
        barcode_id = intent.getString("UserEmail");*/

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
       // mViewPager.setAdapter(mSectionsPagerAdapter);
        setupViewPager(mViewPager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

        //PASS OVER THE BUNDLE TO OUR FRAGMENT
       // Book_detail myFragment = new Book_detail();
        //myFragment.setArguments(bundle);
       /* nameTxt.setText("");
        launchYearSpinner.setSelection(0);*/
        //THEN NOW SHOW OUR FRAGMENT
       // getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,myFragment).commit();

    }

    public static void setData(String barcode_id, String aResult, String book_id) {
        data.add(new NoteModel(barcode_id, aResult, book_id));
    }
    public static ArrayList<NoteModel> getData() {
        return data;
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.badge_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.user);
        tabLayout.getTabAt(2).setIcon(R.drawable.search);
        int paramInt =10;
        BadgeDrawable badgeDrawable = null;
      //  BitmapDrawable iconBitmap = (BitmapDrawable) tabLayout.getTabAt(0).getIcon();
        LayerDrawable localLayerDrawable =(LayerDrawable)tabLayout.getTabAt(0).getIcon();
   /*     Drawable cartBadgeDrawable = localLayerDrawable
                .findDrawableByLayerId(R.id.ic_badge);
        if ((cartBadgeDrawable != null)
                && ((cartBadgeDrawable instanceof BadgeDrawable))
                && (paramInt < 10)) {
            badgeDrawable = (BadgeDrawable) cartBadgeDrawable;
            Log.d("gg","hh");
        } else {
            badgeDrawable = new BadgeDrawable(this);
            Log.d("bj","gh");
        }*/
        badgeDrawable = new BadgeDrawable(this);
        badgeDrawable.setCount(800000);
        localLayerDrawable.mutate();
        localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
        tabLayout.getTabAt(0).setIcon(localLayerDrawable);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Book_detail(), "CheckOut");
        adapter.addFragment(new Profile(), "Profile");
        adapter.addFragment(new SettingAccount(), "Search");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
           /* if(fragment instanceof Book_detail){
                bundle = new Bundle();
                bundle.putString("NAME_KEY", barcode_id.toString());
                Log.d("id",barcode_id.toString());
                fragment.setArguments(bundle);
            }
*/
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.Logout) {
            Intent a = new Intent(MainActivity.this,Login.class);
            startActivity(a);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
//    public class SectionsPagerAdapter extends FragmentPagerAdapter {
//
//        public SectionsPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            switch (position)
//            {
//                case 0:
//                    Book_detail book_detail = new Book_detail();
//                    return book_detail;
//                case 1:
//                    Profile profile = new Profile();
//                    return profile;
//                case 2:
//                    SettingAccount settingAccount = new SettingAccount();
//                   return settingAccount;
//                case 3:
//                    Book_detail book_detail1 = new Book_detail();
//                    return book_detail1;
//                default:
//                    return null;
//
//
//            }
//        }
//
//        @Override
//        public int getCount() {
//            // Show 3 total pages.
//            return 4;
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            switch (position) {
//                case 0:
//                    return "Book Detail";
//                case 1:
//                    return "Profile";
//                case 2:
//                    return "Setting";
//                case 3:
//                    return "book";
//                default:
//                    return null;
//            }
//           // return null;
//        }
//    }

