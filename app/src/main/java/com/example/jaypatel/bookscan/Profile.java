package com.example.jaypatel.bookscan;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jay patel on 3/6/2018.
 */


public class Profile extends Fragment {
    String barcode_id,user_name,user_Email;
    TextView section_label;

    List<Book_details> booklist;
    TextView user_Name,user_barcode,user_email;
    ImageView barcode;
    public static final String mypreference = "mypref";
    SharedPreferences sharedpreferences;
    public static final String Barcode = "barcode";
    String barcodenumber;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile, container, false);

        /*section_label = (TextView)rootView.findViewById(R.id.section_label);

        barcode_id = this.getArguments().getString("NAME_KEY").toString();

        section_label.setText(barcode_id);*/
        user_Name = (TextView)rootView.findViewById(R.id.name);
        user_barcode = (TextView)rootView.findViewById(R.id.user_barcode);
        barcode = (ImageView)rootView.findViewById(R.id.barcode);
        user_email = (TextView)rootView.findViewById(R.id.user_email);






        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Barcode)) {
            barcodenumber=sharedpreferences.getString(Barcode, "");
            Log.d("barcodenumber",barcodenumber);
        }

        ApiService service = api.getClient().create(ApiService.class);
        Call<DataModel> call = service.getUser(barcodenumber);
        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                user_name = response.body().getUser_data().get(0).getFirst_name();
                user_Email = response.body().getUser_data().get(0).getUser_email();
                Log.d("daaa",response.body().getUser_data().get(0).getUser_barcode_id());
                user_Name.setText(user_name);
                user_email.setText(user_Email);
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                Log.d("error",t.getMessage());
           }
       });

        user_barcode.setText(barcodenumber);


        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(barcodenumber, BarcodeFormat.CODE_128,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            barcode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return rootView;
    }
}

