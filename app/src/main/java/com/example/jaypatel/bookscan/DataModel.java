package com.example.jaypatel.bookscan;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 12-03-2018.
 */
public class DataModel {


    @SerializedName("user_data")
    private List<Book_details> user_data;

    public List<Book_details> getUser_data() {
        return user_data;
    }

    public void setUser_data(List<Book_details> user_data) {
        this.user_data = user_data;
    }

    private List<Book_details> data;

    public List<Book_details> getData() {
        return data;
    }

    public void setData(List<Book_details> data) {
        this.data = data;
    }
}
